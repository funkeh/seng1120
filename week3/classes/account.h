class account {

  public:

    account();
    account(int open_amt);

    void deposit(int amount);
    void withdraw(int amount);

    int balance() const;
    bool has_funds() const;

    void operator += (const account& acc2);

  private:

    int acct_balance;

};
