// SENG1120
// Josef Frank, c3162099
// Assignment 3

#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include "DBInterface.h"

namespace josef_a3 {

	// Constructor
	DBInterface::DBInterface() { }

	// Destructor
	DBInterface::~DBInterface() { }

	// Core function
	bool DBInterface::run() {

		// Variable in which user input will be stored
		std::string input;

		// Display options for user
		std::cout << "Please select one of the following options:" << std::endl
		          << "1) Read current inventory file" << std::endl
		          << "2) Read/exec monthly update file" << std::endl
		          << "3) Display statistics" << std::endl
		          << "4) Search list items file" << std::endl
		          << "5) Quit" << std::endl
		          << "> ";

		// Store input from user
		std::getline(std::cin, input);

		// Interpret user input and offer feedback if input is invalid
		if (input == "1")
			readInventory();
		else if (input == "2")
			readMonthlyUpdate();
		else if (input == "3")
			displayStatistics();
		else if (input == "4")
			searchListItems();
		else if (input == "5")
			return false;	// End loop as user wills it
		else
			std::cout << "Please type a number between 1 and 5" << std::endl;

		// Continue to loop if program has not been interrupted
		return true;
	}


	void DBInterface::readInventory() {

		std::string   file_name;  // File to be read

		// Temporary binary tree
		tree = new BSTree<T>();

		// Obtain file name
		std::cout << "Which file should be read?" << std::endl << "> ";
		std::cin >> file_name;

		interpretFile(tree, file_name);
	}

	void DBInterface::readMonthlyUpdate() {

		std::string   file_name;  // File to be read

		// Obtain file name
		std::cout << "Which file should be read?" << std::endl << "> ";
		std::cin >> file_name;

		interpretFile(tree, file_name);
	}

	void DBInterface::displayStatistics() {

		std::cout << "Number of different parts: "
				  << tree->size() << std::endl;
		std::cout << "Total number of parts: "
				  << tree->total() << std::endl;
	}

	void DBInterface::searchListItems() {

		std::string   file_name;  // File to be read
		std::string   line;       // Stores lines read from file
		std::ifstream str_file;   // Filestream for file

		int num_comp = 0;
		int num_lines = 0;

		double average;

		// Obtain file name
		std::cout << "Which file should be read?" << std::endl << "> ";
		std::cin >> file_name;


		// Open file
		str_file.open(file_name.c_str());

		// Read all lines from file
		if (str_file.is_open()) {

			// Load one line to get rid of header
			std::getline(str_file, line);

			while (std::getline(str_file, line)) {

				if (line.substr(line.size() - 1, 1).compare("\r") == 0)
					line = line.substr(0, line.size() - 1);

				std::cout << std::left << std::setw(20) << line
					      << tree->getQuantity(line) << std::endl;

				num_lines++;
				num_comp += tree->comps(line);
			}

			average = num_comp / (double)num_lines;

			std::cout << "Average num of comparisons: " << average << std::endl;

			// Close file
			str_file.close();
		}
		else
			std::cout << "File could not be opened." << std::endl;


		// Read one more line so that the program doesn't cry
		std::getline(std::cin, file_name);

	}

	void DBInterface::interpretFile(BSTree<T>* tree, std::string file_name) {

		std::ifstream str_file;   // Filestream for file
		std::string   line;       // Stores lines read from file

		// Open file
		str_file.open(file_name.c_str());

		// Read all lines from file
		if (str_file.is_open()) {

			// Load one line to get rid of header
			std::getline(str_file, line);

			while (std::getline(str_file, line)) {

				std::stringstream line_stream(line);  // Stream to split with
				std::string       code;               // Code from split
				int               quantity;           // Quantity from split

				// Load data, seperated by tab
				std::getline(line_stream, code, '\t');

				line_stream >> quantity;

				if (quantity > 0)
					tree->insert(code, quantity);
				else
					tree->subtract(code, -quantity);
			}

			// Close file
			str_file.close();
		}
		else
			std::cout << "File could not be opened." << std::endl;


		// Read one more line so that the program doesn't cry
		std::getline(std::cin, file_name);
	}
}
