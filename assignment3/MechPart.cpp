// SENG1120
// Josef Frank, c3162099
// Assignment 3

#include <iostream>
#include <string>
#include "MechPart.h"

namespace josef_a3 {

	// Constructors

	MechPart::MechPart() {

		code = "";
		quantity = 0;
	}

	MechPart::MechPart(const S& init_code) {

		code = init_code;    // Stored data
		quantity = 0;
	}

	MechPart::MechPart(const S& init_code, const int& init_quantity) {

		code = init_code;           // Stored data
		quantity = init_quantity;   // Node which this is linked to
	}

	// Destructor
	MechPart::~MechPart() { }

	// Member functions

	S MechPart::getCode() const {

		return code;
	}

	int MechPart::getQuantity() const {

		return quantity;
	}

	void MechPart::modifyQuantity(const int num) {

		quantity += num;
	}

	void MechPart::setCode(const S& new_code) {

		code = new_code;
	}

	void MechPart::setQuantity(const int& new_quantity) {

		quantity = new_quantity;
	}
};
