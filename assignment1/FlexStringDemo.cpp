// SENG1120
// Josef Frank, c3162099
// Assignment 1

#include <cstdlib>
#include <iostream>
#include <string>
#include "FlexString.h"

using namespace std;
using namespace josef_a1;

int main() {

  typedef FlexString::value_type value_type;

  // Word to be input by the user
  value_type word;

  // Create FlexStrings to store text inside of
  FlexString* inputOne = new FlexString();
  FlexString* inputTwo = new FlexString();

  // Print an introduction to the program
  cout << "SENG1120 Assignment 1" << endl;
  cout << "Josef Frank, c3162099" << endl;


  // Part 1 start
  cout << endl << "+ Part 1" << endl << endl;

  // Collect input from user and store in inputOne
  cout << "Please input first sentence:" << endl << "> ";
  cin >> *inputOne;

  // Print the stored input
  cout << "inputOne is: " << *inputOne << endl;


  // Part 2 start
  cout << endl << "+ Part 2" << endl << endl;

  // Print information about inputOne
  cout << "inputOne contains " << inputOne->length() << " words" << endl;
  cout << "The word \"car\" appears " << inputOne->count("car") <<
          " times in inputOne" << endl;


  // Part 3 start
  cout << endl << "+ Part 3" << endl << endl;

  // Collect input from user and store in inputTwo
  cout << "Please input second sentence:" << endl << "> ";
  cin >> *inputTwo;

  // Add the contents of inputTwo to inputOne
  *inputOne += *inputTwo;

  // Print the stored input
  cout << "inputOne is: " << *inputOne << endl;
  cout << "inputTwo is: " << *inputTwo << endl;


  // Part 4 start
  cout << endl << "+ Part 4" << endl << endl;

  // Print information about inputOne
  cout << "inputOne contains " << inputOne->length() << " words" << endl;
  cout << "The word \"is\" appears " << inputOne->count("is") <<
          " times in inputOne" << endl;


  // Part 5 start
  cout << endl << "+ Part 5" << endl << endl;

  // Collect input from user and store in word
  cout << "Please input word to be removed from inputTwo:" << endl << "> ";
  cin >> word;

  // Remove all instances of words from inputTwo
  inputTwo->remove(word);

  // Print the stored input
  cout << "inputOne is: " << *inputOne << endl;
  cout << "inputTwo is: " << *inputTwo << endl;


  return 0;
}
