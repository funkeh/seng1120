// SENG1120
// Josef Frank, c3162099
// Assignment 2

#include <cstdlib>
#include <iostream>
#include <string>
#include "TowerHanoi.h"

int main() {

	// Create new game object
	josef_a2::TowerHanoi* game = new josef_a2::TowerHanoi();

	// Whether game will continue looping
	bool loop;

	// Run game until user indicates they want it to end
	do {

		loop = game->performGame();
	} while(loop);
}
