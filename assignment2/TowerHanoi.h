// SENG1120
// Josef Frank, c3162099
// Assignment 2

#ifndef JOSEF_TOWERHANOI_H
#define JOSEF_TOWERHANOI_H

#include "LStack.h"

namespace josef_a2 {
	class TowerHanoi {

		typedef std::string T;

		public:

			TowerHanoi();   // Constructor
			~TowerHanoi();  // Destructor

			// Starts the main game loop
			// OUTPUT: Whether the user wants the game to restart (bool).
			bool performGame();

			// Prints out all three towers
			void printTowers();

			// Returns the input as an integer if between 1 or max_value,
			// otherwise it returns -1
			// INPUT:  Input value (T), upper limit (int)
			// OUTPUT: Converted value or -1 (int)
			int processInput(T input, int max_value);

		private:

			int max_height;			// Maximum height of towers
			LStack<T>* tower[3];	// Array of three LStacks for storing towers
	};
};

#endif
