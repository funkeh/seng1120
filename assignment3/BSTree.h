// SENG1120
// Josef Frank, c3162099
// Assignment 3

#ifndef JOSEF_BSTREE_H
#define JOSEF_BSTREE_H

#include "BTNode.h"

namespace josef_a3 {

	template <typename T>
	class BSTree {

	public:

		// Constructor of BSTree
		BSTree();

		// Destructor of BSTree
		~BSTree();

		// Insert a node after current.
		// INPUT:	Node to insert.
		void insert(const std::string data, const int quantity);

		void add(BTNode<T>* node, const std::string& data, const int& quantity);

		void subtract(const std::string data, const int quantity);

		bool remove(const std::string code);

		size_t comps(const std::string data);

		size_t getComparisons(BTNode<T>* node, const std::string data);

		size_t getQuantity(const std::string data);

		// Returns a count of the nodes in the list.
		// OUTPUT:	A count of the nodes in the list, in size_t.
		size_t size();

		// Returns a count of the nodes in the list.
		// OUTPUT:	A count of the nodes in the list, in size_t.
		size_t total();
	private:

		int count;
		int num;
		BTNode<T>* root;

		BTNode<T>* search(BTNode<T>* node, const std::string data);
	};
};

#include "BSTree.template"

#endif
