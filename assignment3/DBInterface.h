// SENG1120
// Josef Frank, c3162099
// Assignment 3

#ifndef JOSEF_DBINTERFACE_H
#define JOSEF_DBINTERFACE_H

#include "MechPart.h"
#include "BSTree.h"

namespace josef_a3 {

	typedef josef_a3::MechPart T;
	class DBInterface {

		public:

			DBInterface();   // Constructor
			~DBInterface();  // Destructor

			void readInventory();
			void readMonthlyUpdate();
			void displayStatistics();
			void searchListItems();

			void interpretFile(BSTree<T>* tree, std::string file_name);

			// Run the program
			// OUTPUT: Whether or not the program will loop again (bool)
			bool run();
		private:

			BSTree<T>* tree;
	};
};

#endif
