// SENG1120
// Josef Frank, c3162099
// Assignment 3

#ifndef JOSEF_MECHPART_H
#define JOSEF_MECHPART_H
#include <string>

namespace josef_a3 {

	typedef std::string S;
	class MechPart {

		public:

			// Constructors for MechPart
			// INPUT:	code is the code to be stored of type T.
			//          quantity is the quantity of the stored MechPart.
			MechPart();
			MechPart(const S& init_code);
			MechPart(const S& init_code, const int& init_quantity);

			// Destructor
			~MechPart();

			// Retrieves the current MechPart's code
			// OUTPUT:	The current code of the MechPart.
			S getCode() const;

			// Retrieves the current MechPart's quantity
			// OUTPUT:	The current quantity of the MechPart.
			int getQuantity() const;

			void modifyQuantity(const int num);

			// Sets the data of the node
			// INPUT:	Data to be stored in MechPart, of type T.
			void setCode(const S& new_code);

			// Sets the quantity of the MechPart
			// INPUT:	Quantity to be stored in MechPart, of type int.
			void setQuantity(const int& new_quantity);

		private:

			S code;			// Code stored in MechPart
			int quantity;	// Quantity of MechPart
	};
};

#endif
