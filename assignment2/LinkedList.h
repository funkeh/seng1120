// SENG1120
// Josef Frank, c3162099
// Assignment 2

#ifndef JOSEF_LINKEDLIST_H
#define JOSEF_LINKEDLIST_H

#include "Node.h"

namespace josef_a2 {

	template <typename T>
	class LinkedList {

	public:

		// Constructor of LinkedList
		LinkedList();

		// Destructor of LinkedList
		~LinkedList();

		// Inserts a node at the head of a LinkedList.
		// INPUT:	Node to insert at head.
		void headInsert(const T& entry);

		// Remove node at the head of a LinkedList.
		void headRemove();

		// Insert a node after current.
		// INPUT:	Node to insert.
		void insert(const T& entry);

		// Returns a count of the nodes in the list.
		// OUTPUT:	A count of the nodes in the list, in size_t.
		int length();

		// Returns the value of the selected position
		// OUTPUT:	Value at position.
		T value();

		// Sets the current Node to head_ptr
		void moveToHead();

		// Sets the current Node to tail_ptr
		void moveToTail();

	private:

		int count;

		Node<T>* head_ptr;  // Head node in LinkedList
		Node<T>* tail_ptr;  // Tail node in LinkedList
		Node<T>* current;   // Current node in LinkedList
	};
};

#include "LinkedList.template"

#endif
