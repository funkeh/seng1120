// SENG1120
// Josef Frank, c3162099
// Assignment 1

#include <iostream>
#include <sstream>
#include <string>
#include "LinkedList.h"

namespace josef_a1 {

  typedef LinkedList::value_type value_type;

  // Constructor

  LinkedList::LinkedList() { }

  // Destructor

  LinkedList::~LinkedList() {

    // Move to start of list
    current = head_ptr;

    // Temporary variable to store Node link
    Node* next;

    // Iterate through all Nodes, deleting until list is empty
    while (current != NULL) {

      next = current->link();

      delete current;

      current = next;

    }
  }

  // Member functions

  int LinkedList::list_count(const value_type& input) {

    // Create local counter
    int count = 0;

    // Iterate through all Nodes
    for (current = head_ptr; current != NULL; current = current->link()) {

      // If Node's data matches the input
      if (input == current->get_data()) {

        // Increase counter
        count++;
      }
    }

    return count;

  }

  void LinkedList::list_head_insert(const value_type& entry) {

    // Set the head to a new node, linking the old head
    head_ptr = new Node(entry, head_ptr);

    // Sets the tail if the list is empty
    if (tail_ptr == NULL) {

      tail_ptr = head_ptr;
    }

    current = head_ptr;
  }

  void LinkedList::list_insert(const value_type& entry) {

    // Create new node with given data and link
    Node* add_ptr = new Node(entry, current->link());

    // Modify current Node's link
    current->set_link(add_ptr);

    // Change tail_ptr if current is tail
    if (current == tail_ptr) {

      tail_ptr = current->link();
    }

    // Move current to next linked Node
    current = current->link();
  }

  size_t LinkedList::list_length() {

    // Variable used to store count of all nodes
    size_t count = 0;

    // Iterate through all nodes and increase count for each one
    for (current = head_ptr; current != NULL; current = current->link()) {

      count++;
    }

    return count;
  }

  int LinkedList::list_remove(const value_type& input) {

    // Counter of number of Nodes removed
    int count = 0;

    // Previous Node in loop
    Node* last;

    // Move to start of list
    current = head_ptr;

    // Iterate through all nodes
    while (current != NULL) {

      if (current->get_data() == input) {

        if (current == head_ptr) {

          // Move head forward
          head_ptr = current->link();

          // If head is null then tail must also
          if (head_ptr == NULL) {

            tail_ptr = NULL;

          }

          // Delete current Node and move forward
          delete current;
          current = head_ptr;
        }

        else if (current == tail_ptr) {

          // Move tail back one
          tail_ptr = last;

          if (tail_ptr != NULL) {

            tail_ptr->set_link(NULL);
          }

          // Delete current Node and set it to null
          delete current;
          current = NULL;
        }

        else {

          // For internal Nodes

          // Link around the Node
          last->set_link(current->link());

          // Delete current and move on
          delete current;
          current = last->link();
        }

        // Increase count of deleted Nodes
        count++;

      }
      else {

        // Move last up and select the next Node
        last = current;
        current = current->link();
      }
    }

    return count;
  }

  value_type LinkedList::list_value() {

    // Value of current list
    value_type data = "";

    // Iterate through all nodes and increase count for each one
    for (current = head_ptr; current != NULL; current = current->link()) {

      if (data != "") {

        data += " ";
      }

      data += current->get_data();
    }

    return data;
  }

  void LinkedList::move_to_tail() {

    current = tail_ptr;

  }
};
