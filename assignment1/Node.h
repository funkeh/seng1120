// SENG1120
// Josef Frank, c3162099
// Assignment 1

#ifndef JOSEF_NODE_H
#define JOSEF_NODE_H

using namespace std;

namespace josef_a1 {

  class Node {

    public:

      typedef string value_type;

      // Constructors for node
      // INPUT: initial_data is the data to be stored in node,
      //          of type value_type.
      //        initial_link is the node to be linked to.
      Node();
      Node(const value_type& data);
      Node(const value_type& data, Node* initial_link);

      // Destructor
      ~Node();

      // Sets the data of the node
      // INPUT: Data the be stored in node, of type value_type.
      void set_data(const value_type& new_data);

      // Sets the node which is linked to
      // INPUT: Node to be linked to.
      void set_link(Node* new_link);

      // Retrieves the current node's data
      // OUTPUT: The current data of the node.
      value_type get_data() const;

      // Link to an already created Node
      // INPUT: Node to be linked to.
      const Node* link() const;
      Node* link();

    private:

      value_type data;  // Data stored in Node
      Node* next;       // Node to be linked to
  };
};

#endif
