#include <iostream>
#include "account.h"
using namespace std;

int main(void) {

  account *acc1 = new account(40);
  account *acc2 = new account(56);

  *acc1 += *acc2;

  cout << acc1->balance() << endl;

  return 0;

}
