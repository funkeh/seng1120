// SENG1120
// Josef Frank, c3162099
// Assignment 1

#include <iostream>
#include <sstream>
#include <string>
#include "FlexString.h"

namespace josef_a1 {

  typedef FlexString::value_type value_type;

  // Constructors

  FlexString::FlexString() {

    // Default values
    wordCount = 0;
    data = "";
  }
  FlexString::FlexString(const value_type& input) {

    // Default values
    wordCount = 0;
    data = "";

    // Store input inside of the LinkedList
    store(input);
  }

  // Destructor

  FlexString::~FlexString() { }

  // Member functions

  int FlexString::count(const value_type& input) {

    return list.list_count(input);
  }

  bool FlexString::empty() {

    return wordCount == 0;
  }

  int FlexString::length() {

    return wordCount;
  }

  void FlexString::remove(const value_type& input) {

    // Return all instances of 'input' from FlexString
    wordCount -= list.list_remove(input);

    // Get new value
    data = list.list_value();
  }

  void FlexString::store(const value_type& input) {

    // Move the LinkedList to the tail
    list.move_to_tail();

    // Create a stringstream to tokenise the input
    stringstream ss(input);

    // Variable in which to store each token temporarily
    Node::value_type word;

    // Loop until there is no data left in the stringstream
    while (ss >> word) {

      // Add space if already contains words
      if (wordCount > 0) {

        data += " ";

        // Store each token inside of the LinkedList
        list.list_insert(word);
      }
      else {

        // Store token inside of the LinkedList
        list.list_head_insert(word);
      }

      // Add word to current sentence
      data += word;

      // Increase word count
      wordCount++;
    }
  }

  value_type FlexString::value() const {

    return data;
  }

  // Overloading operators

  void FlexString::operator += (const FlexString& input) {

    store(input.value());
  }

  bool operator == (const FlexString &leftFlex, const FlexString &rightFlex) {

    return leftFlex.value() == rightFlex.value();
  }

  ostream& operator << (ostream &out, FlexString &input) {

    out << input.value();

    return out;
  }

  istream& operator >> (istream &in, FlexString &input) {

    // Store the contents of cin in inputData
    string inputData;
    getline(cin, inputData);

    // Store inputData inside of the LinkedList
    input.store(inputData);

    return in;
  }
};
