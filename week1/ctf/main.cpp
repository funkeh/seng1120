/* SENG1120
 * Josef Frank, c3162099
 * Celsius to Fahrenheit
 */

#include<iostream>
using namespace std;

int main() {

  int celMin, celMax;

  // Prompt user for minimum and maximum
  cout << "Min Max: ";
  cin >> celMin >> celMax;

  // Print title
  printf("%-20s%-20s\n", "Celsius", "Fahrenheit");

  // Print horizonal bar
  for (int i = 0; i < 40; i ++) cout << "-";
  cout << endl;

  // Iterate through all possible values of celsius
  for (double d = celMin; d <= celMax; d++) {

    // Print celsius and fahrenheit formatted
    printf("%-20.2f%-20.2f\n", d, d * 2 + 30);
  }

  return 0;

}
