// SENG1120
// Josef Frank, c3162099
// Assignment 2

#include <iostream>
#include <sstream>
#include <string>
#include "TowerHanoi.h"

namespace josef_a2 {

	// Constructor
	TowerHanoi::TowerHanoi() { }

	// Destructor
	TowerHanoi::~TowerHanoi() { }

	bool TowerHanoi::performGame() {

		bool loop_game;    // Will game keep asking for input?
		T input;           // Used to store strings collected in loops

		// Repeat until we have been given a legitimate height
		do {

			// Query the user for information, and then collect it
			std::cout << "How tall are the towers? (Input value from 1 to 5): ";
			std::getline(std::cin, input);

			// Convert the input to an integer
			max_height = processInput(input, 5);
		} while (max_height == -1);

		// Give the user output
		std::cout << "- Towers will be of size " << max_height << "\n";

		// Create three LStacks for tower array
		for (int i = 0; i < 3; i++)
			tower[i] = new LStack<T>();

		// Possible fill values for left tower
		T values[5] = {
			"     X     ",
			"    XXX    ",
			"   XXXXX   ",
			"  XXXXXXX  ",
			" XXXXXXXXX "
		};

		// Fill values for left tower with strings
		for (int i = max_height; i > 0; i--)
			tower[0]->push(values[i - 1]);

		// Print initial state of towers
		printTowers();

		//
		do {

			// Query the user for information, and then collect it
			std::cout << "Please input moves in 'x x' format (0 0 ends game): ";
			std::getline(std::cin, input);

			// Default state is to loop game
			loop_game = true;

			// Error checking
			if (input == "0 0")
				loop_game = false;
			else if (input.length() != 3)
				std::cout << "Input is not 3 characters long\n";
			else if (input.at(1) != ' ')
				std::cout << "Input is not separated by a space\n";
			else {

				// Integers for processing input into
				int value_a, value_b;

				// Process characters into strings
				std::string str_a, str_b;
				str_a = input.at(0);
				str_b = input.at(2);

				// Process values into integers or errors
				value_a = processInput(str_a, 3);
				value_b = processInput(str_b, 3);

				// Error checking
				if (value_a == -1 || value_b == -1) {

					std::cout << "One or both values are invalid\n";
				}
				else if (value_a == value_b) {

					std::cout << "Both towers are the same\n";
				}
				else if (tower[value_a - 1]->empty()) {

					std::cout << "Origin tower is empty\n";
				}
				else {

					// If second tower has size...
					if (tower[value_b - 1]->size() > 0) {

						// Get top values
						std::string inp_a, inp_b;
						inp_a = tower[value_a - 1]->top();
						inp_b = tower[value_b - 1]->top();

						// If tower 1's top is larger than tower 2's top...
						if (inp_a.compare(inp_b) > 0) {

							// Print error, continue
							std::cout << "Invalid move\n";
							continue;
						}
					}

					// Move value from top of tower 1 to tower 2
					tower[value_b - 1]->push(tower[value_a - 1]->top());
					tower[value_a - 1]->pop();

					printTowers();

					// If third tower is filled...
					if (tower[2]->size() == max_height) {

						// ...print win!
						std::cout << "You win!\n";
						loop_game = false;
					}
				}
			}
		} while (loop_game);

		// Ask player if they'd like to play again
		std::cout << "Would you like to play again? (y for yes, anything " <<
			"else means no): ";
		std::getline(std::cin, input);

		return (input == "y");
	}


	void TowerHanoi::printTowers() {

		// Temporary array  of towers
		LStack<T>* tmpTowers[3];
		int size[3];

		// Copy values from main array to temporary array
		for (int i = 0; i < 3; i++) {
			size[i] = tower[i]->size();
			tmpTowers[i] = new LStack<T>[tower[i]->size()];
		}

		// Iterate through all rows
		for (int i = max_height; i > 0; i--) {

			// Iterate through all towers
			for (int j = 0; j < 3; j++) {

				// If tower size is id of current rows
				if (tower[j]->size() == i) {

					// Insert tower value into tmp tower
					tmpTowers[j]->push(tower[j]->top());

					// Print top value of tower and pop
					std::cout << tower[j]->top();
					tower[j]->pop();
				}
				else {

					// Insert blank space
					std::cout << "           ";
				}
			}

			// Print new line to separate rows
			std::cout << std::endl;
		}

		// Copy values from temporary array back to main array
		for (int i = 0; i < 3; i++) {

			for (int j = 0; j < size[i]; j++) {

				tower[i]->push(tmpTowers[i]->top());
				tmpTowers[i]->pop();
			}
		}

		// Print the footer
		std::cout << "---------------------------------\n";
		std::cout << "     1          2          3     \n";
	}


	int TowerHanoi::processInput(T input, int max_value) {

		// Converted value of input
		int value;

		// Stringstream for conversion
		std::stringstream stream(input);

		// If conversion fails, return error
		if (!(stream >> value)) {

			std::cout << "Input cannot be interpretted\n";
			return -1;
		}
		// If value is out of range, return error
		else if (value < 1 || value > max_value) {

			std::cout << "Input is out of range (1 to " << max_value << ")\n";
			return -1;
		}
		// Return converted value if no errors found
		else {

			return value;
		}
	}
}
