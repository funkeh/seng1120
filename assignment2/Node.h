// SENG1120
// Josef Frank, c3162099
// Assignment 2

#ifndef JOSEF_NODE_H
#define JOSEF_NODE_H

namespace josef_a2 {

	template <typename T>
	class Node {

		public:

			// Constructors for node
			// INPUT:	initial_data is the data to be stored in node,
			//			of type value_type.
			//			initial_link is the node to be linked to.
			Node();
			Node(const T& data);
			Node(const T& data, Node<T>* initial_link);

			// Destructor
			~Node();

			// Retrieves the current node's data
			// OUTPUT:	The current data of the node.
			T getData() const;

			// Link to an already created Node
			// OUTPUT:	Node to be linked to.
			const Node<T>* getLink() const;
			Node<T>* getLink();

			// Sets the data of the node
			// INPUT:	Data the be stored in node, of type value_type.
			void setData(const T& new_data);

			// Sets the node which is linked to
			// INPUT:	Node to be linked to.
			void setLink(Node<T>* new_link);

		private:

			T data;			// Data stored in Node
			Node<T>* next;	// Node to be linked to
	};
};

#include "Node.template"

#endif
