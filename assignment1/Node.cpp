// SENG1120
// Josef Frank, c3162099
// Assignment 1

#include <iostream>
#include <string>
#include "Node.h"
#include "FlexString.h"

namespace josef_a1 {

  typedef Node::value_type value_type;

  // Constructors

  Node::Node() { }
  Node::Node(const value_type& initial_data) {

    data = initial_data;    // Stored data
  }
  Node::Node(const value_type& initial_data, Node* initial_link) {

    data = initial_data;    // Stored data
    next = initial_link;    // Node which this is linked to
  }

  // Destructor

  Node::~Node() { }

  // Member functions

  value_type Node::get_data() const {

    return data;
  }

  const Node* Node::link() const {

    return next;
  }
  Node* Node::link() {

    return next;
  }

  void Node::set_data(const value_type& new_data) {

    data = new_data;
  }

  void Node::set_link(Node* new_link) {

    next = new_link;
  }
};
