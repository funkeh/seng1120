/* SENG1120
 * Josef Frank, c3162099
 * Exception handling
 */

#include <iostream>
using namespace std;

// Error code constants - unsigned chars for low memory impact
const unsigned char ERR_NEGATIVE_AGE = 1;

int main() {

  int studentAge;

  // Ask user for student age
  cout << "Enter student age: ";
  cin >> studentAge;

  // Error testing
  try {

    // If negative studentAge, throw exception
    if (studentAge < 0) throw(ERR_NEGATIVE_AGE);

    // Print age if exception not thrown
    cout << "\nStudent age: " << studentAge << endl;

  }
  catch (unsigned char error) {

    // Interpret error message
    switch(error) {
      case ERR_NEGATIVE_AGE:
        cout << "Student age is negative." << endl;
        return -1;
    }

  }
  return 0;

}
