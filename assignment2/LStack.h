// SENG1120
// Josef Frank, c3162099
// Assignment 2

#ifndef JOSEF_LSTACK_H
#define JOSEF_LSTACK_H

#include "LinkedList.h"

namespace josef_a2 {

	template <typename T>
	class LStack {

		public:

			LStack();		// Constructor
			~LStack();		// Destructor

			// Returns whether or not the LStack is empty
			// OUTPUT: false if not empty, true if so.
			bool empty() const;

			// Removes the top value on the LStack
			void pop();

			// Push value to the top of LStack
			// INPUT: String to remove.
			void push(const T& input);

			// Returns a count of the words in LStack
			// OUTPUT: Count of words (in int).
			int size() const;

			// Returns the top value on the LStack
			// OUTPUT: Value of the top of the LStack.
			T top() const;

		private:

			LinkedList<T>* list;    // Private LinkedList
	};
};

#include "LStack.template"

#endif
