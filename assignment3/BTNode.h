// SENG1120
// Josef Frank, c3162099
// Assignment 3

#ifndef JOSEF_BTNODE_H
#define JOSEF_BTNODE_H

#include "MechPart.h"

namespace josef_a3 {

	template <typename T>
	class BTNode {

		public:

			// Constructors for node
			// INPUT:  initial_data is the data to be stored in node,
			//         of type value_type.
			//         initial_link is the node to be linked to.
			BTNode();
			BTNode(const std::string code, const int quantity);

			// Destructor
			~BTNode();

			// Retrieves the current node's data
			// OUTPUT: The current data of the node.
			T getData() const;

			std::string getCode() const;

			int getQuantity() const;

			void modifyQuantity(const int num);

			// Link to an already created Node
			// OUTPUT: BTNode to be linked to.
			const BTNode<T>* getLeft() const;
			BTNode<T>* getLeft();

			// Link to an already created Node
			// OUTPUT: BTNode to be linked to.
			const BTNode<T>* getRight() const;
			BTNode<T>* getRight();

			// Sets the data of the node
			// INPUT:  Data the be stored in node, of type T.
			void setData(const T& new_data);

			// Sets the node which is linked to
			// INPUT:  BTNode to be linked to.
			void setLeft(BTNode<T>* new_link);

			// Sets the node which is linked to
			// INPUT:  BTNode to be linked to.
			void setRight(BTNode<T>* new_link);

		private:

			T* data;            // Data stored in BTNode
			BTNode<T>* left;    // BTNode to be linked to
			BTNode<T>* right;   // BTNode to be linked to
	};
};

#include "BTNode.template"

#endif
