/* SENG1120
 * Josef Frank, c3162099
 * Hello world
 */

#include <iostream>
using namespace std;

int main() {

  string str1 = "Hello World!";
  cout << str1 << endl;
  return 0;

}
