#include "account.h"

account::account() {

  acct_balance = 0;

}

account::account(int open_amt) {

  acct_balance = open_amt;

}

void account::deposit(int amount) {

  acct_balance += amount;

}

void account::withdraw(int amount) {

  acct_balance -= amount;

}

int account::balance() const {

  return (acct_balance);

}

bool account::has_funds() const {

  return (acct_balance > 0);

}

void account::operator += (const account& acc2) {

  acct_balance += acc2.balance();

}
