// SENG1120
// Josef Frank, c3162099
// Assignment 3

#include <cstdlib>
#include <iostream>
#include <string>
#include "DBInterface.h"

int main() {

	// Create new core object
	josef_a3::DBInterface* core = new josef_a3::DBInterface();

	// Whether game will continue looping
	bool loop;

	// Run game until user indicates they want it to end
	do {

		loop = core->run();
	} while (loop);
}
