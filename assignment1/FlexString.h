// SENG1120
// Josef Frank, c3162099
// Assignment 1

#ifndef JOSEF_FLEXSTRING_H
#define JOSEF_FLEXSTRING_H

#include "LinkedList.h"

using namespace std;

namespace josef_a1 {
  class FlexString {

    public:

      typedef LinkedList::value_type value_type;

      // Constructors, with a potential default value
      FlexString();
      FlexString(const value_type& input);

      // Destructor
      ~FlexString();

      // Returns a count of a specific word in FlexString
      // INPUT: String to count.
      // OUTPUT: Count of a specific word (in int).
      int count(const value_type& input);

      // Returns whether or not the FlexString is empty
      // OUTPUT: false if not empty, true if so.
      bool empty();

      // Returns a count of the words in FlexString
      // OUTPUT: Count of words (in int).
      int length();

      // Removes all instances of a given word from a FlexString
      // INPUT: String to remove.
      void remove(const value_type& input);

      // Stores input inside of FlexString
      // INPUT: Value to be stored inside of FlexString.
      void store(const value_type& input);

      // Returns the sentence contained in the FlexString
      // OUTPUT: Value of the FlexString.
      value_type value() const;

      // Insert the contents from a given FlexString into self
      void operator += (const FlexString& input);

    private:

      int wordCount;    // Number of words in FlexString
      LinkedList list;  // Private LinkedList
      value_type data;  // Contained sentence
  };

  // Returns whether the values of two FlexStrings are the same
  bool operator == (const FlexString &leftFlex, const FlexString &rightFlex);

  // Outputs the contents of FlexString for printing
  ostream& operator << (ostream &out, FlexString &input);

  // Input a string into FlexString
  istream& operator >> (istream &in, FlexString &input);
};

#endif
