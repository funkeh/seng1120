// SENG1120
// Josef Frank, c3162099
// Assignment 1

#ifndef JOSEF_LINKEDLIST_H
#define JOSEF_LINKEDLIST_H

#include "Node.h"

using namespace std;

namespace josef_a1 {
  class LinkedList {

    public:

      typedef Node::value_type value_type;

      // Constructor of LinkedList
      LinkedList();

      // Destructor of LinkedList
      ~LinkedList();

      // Counts number of Nodes with given value
      // INPUT: Value of Nodes to be counted.
      // OUTPUT: Number of Nodes counted.
      int list_count(const value_type& input);

      // Inserts a node at the head of a LinkedList.
      // INPUT: Node to insert at head.
      void list_head_insert(const value_type& entry);

      // Insert a node after current.
      // INPUT: Node to insert.
      void list_insert(const value_type& entry);

      // Returns a count of the nodes in the list.
      // OUTPUT: A count of the nodes in the list, in size_t.
      size_t list_length();

      // Removes all Nodes of a given value from the list
      // INPUT: Value of Nodes to be removed.
      // OUTPUT: Number of Nodes removed.
      int list_remove(const value_type& input);

      // Returns the value of the list
      // OUTPUT: Accoumulated list value.
      value_type list_value();

      // Sets the current Node to tail_ptr
      void move_to_tail();

    private:

      Node* head_ptr;   // Head node in LinkedList
      Node* tail_ptr;   // Tail node in LinkedList
      Node* current;    // Current node in LinkedList
  };
};

#endif
